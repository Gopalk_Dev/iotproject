package com.app.main.controller;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins =  "*", allowedHeaders = "*")
@RestController
public class Controller {

    Map<String, Map> database = new HashMap<>();

    @GetMapping("/getFromDb/{project}/{key}")
    public String getFromDb(@PathVariable String project, @PathVariable String key){
        String value = "null";

        Map<String, String> table = database.get(project);
        if(table != null && table.get(key) != null)
            value = table.get(key);

        return value;
    }

    @GetMapping("/setToDb/{project}/{key}/{value}")
    public String setToDb(@PathVariable String project, @PathVariable String key, @PathVariable String value) {
        Map<String, String> table = database.get(project);

        if(table == null)
            table = new HashMap<String, String>();

        table.put(key, value);
        database.put(project, table);

        return "true";
    }

    @GetMapping("/deleteFromDb/{project}")
    public String deleteFromDb(@PathVariable String project){
        database.remove(project);
        return "true";
    }

    @GetMapping("/getAllFromDb")
    public Object getAllValues(){
        return database;
    }

    @GetMapping("/resetDb")
    public String resetDb(){
        database.clear();
        return "true";
    }
}
